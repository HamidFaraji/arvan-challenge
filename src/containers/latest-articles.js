import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import queryString from 'query-string';

import ArticlesList from 'components/articles-list';
import Paging from 'components/paging';
import Loading from 'components/loading';

@inject('articleStore')
@observer class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1
    };
  }

  loadArticles(props) {
    const { articleStore, location, ...otherProps } = props;
    const { page, tag, ...query } = queryString.parse(location.search);
    const author = otherProps.author || query.author;

    articleStore.loadArticles(page, author, tag);

    this.setState({
      page
    });
  }

  componentDidMount() {
    this.loadArticles(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.loadArticles(newProps);
  }

  render() {
    const { articleStore } = this.props;
    const { page } = this.state;

    if (articleStore.pending) {
      return <Loading />;
    }

    return (
      <div className="latest-articles">
        <ArticlesList
          list={articleStore.list}
          likes={articleStore.liked}
          loading={articleStore.pending}
          onLike={(slug) => articleStore.likeArticle(slug)}
        />
        <Paging
          count={articleStore.allCount}
          active={page}
        />
      </div>
    );
  }
}

export default Home;
