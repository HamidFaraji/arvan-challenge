import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Container
} from 'reactstrap';

@inject('authStore')
@observer class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  renderAuthButtons() {
    return (
      <Fragment>
        <NavItem>
          <NavLink tag={Link} to="/auth/login">Login</NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to="/auth/register" className="item--pill">Register</NavLink>
        </NavItem>
      </Fragment>
    );
  }

  renderUserButtons() {
    const { authStore } = this.props;
    return (
      <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
          <span>Hello: </span>
          <strong>{authStore.username}</strong>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem onClick={() => this.props.authStore.logout()}>Logout</DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    );
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    const { authStore } = this.props;

    return (
      <Navbar color="dark" expand="md" className="main-header">
        <Container>
          <NavbarBrand tag={Link} to="/">Arvan Cloud Challenge</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              {
                authStore.isLogin ? this.renderUserButtons() : this.renderAuthButtons()
              }
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}

export default Header;
