import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import Loading from 'components/loading';

export default ({ list, loading }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <ListGroup className="tags-list">
      {
        list.map((item, index) => (
          <ListGroupItem tag="a" href={`?tag=${item}`} key={index} className="tags-list__item">{item}</ListGroupItem>
        ))
      }
    </ListGroup>
  );
};
