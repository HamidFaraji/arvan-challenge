import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Redirect } from 'react-router-dom';

function authorize(location = '') {
  return PrivateComponent => {
    @inject('authStore')
    @observer class PrivateRoute extends Component {
      constructor(props) {
        super(props);
      }

      render() {
        const { authStore, ...props } = this.props;

        if (!authStore.isLogin) {
          return (
            <Redirect
              to={{
                pathname: '/auth/login',
                state: {
                  afterLogin: location
                }
              }}
            />
          );
        }

        return <PrivateComponent {...props} />;
      }
    }

    return PrivateRoute;
  };
}

export default authorize;
