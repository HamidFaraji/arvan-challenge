import React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import { Card, CardTitle, CardText, Button, CardSubtitle } from 'reactstrap';
import Loading from 'components/loading';

export default observer(({ list, onLike, likes, loading }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <article className="articles-list">
      {
        list.map((item, index) => (
          <Card
            body
            key={index}
            className="articles-list__item"
          >
            <CardTitle>{item.title}</CardTitle>
            <CardSubtitle>
              <span className="articles-list__author">
                <span>By: </span>
                <strong>
                  <Link to={`/user/@${item.author.username}`}>
                    {item.author.username}
                  </Link>
                </strong>

              </span>
              <span className="articles-list__date">
                <span>Date: </span>
                <strong>
                  {new Date(item.createdAt).toDateString()}
                </strong>

              </span>
            </CardSubtitle>
            <div className="articles-list__body">
              <CardText>{item.description}</CardText>
            </div>
            <div className="clearfix">
              <Button
                outline
                tag={Link}
                to={`/article/${item.slug}`}
                className="articles-list__read-more-btn"
              >Read More...</Button>
              {
                onLike && <Button
                  color="danger"
                  className="articles-list__like-btn"
                  onClick={() => onLike(item.slug)}
                  disabled={item.favorited || likes.indexOf(item.slug) > -1}
                >
                  <i className="material-icons md-24">
                    {item.favorited || likes.indexOf(item.slug) > -1 ? 'favorite' : 'favorite_border'}
                  </i>
                </Button>
              }
            </div>
          </Card>
        ))
      }
    </article>
  );
});
