import React from 'react';
import loadingImage from 'app/assets/images/spinner.svg';

export default () => (
  <div className="text-center">
    <img src={`/${loadingImage}`} />
  </div>
);