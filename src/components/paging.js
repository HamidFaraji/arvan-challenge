import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export default class Paging extends Component {
  static defaultProps = {
    active: 1
  }

  generatePages() {
    const pageCount = Math.round(this.props.count / 10);

    return Array.from(new Array(pageCount));
  }

  render() {
    const { active } = this.props;
    const pages = this.generatePages();

    return (
      <Pagination>
        {
          pages.map((item, index) => {
            const page = index + 1;

            return (
              <PaginationItem active={active == page} key={index}>
                <PaginationLink tag={Link} to={`./?page=${page}`}>{page}</PaginationLink>
              </PaginationItem>
            )
          })
        }
      </Pagination>
    );
  }
}