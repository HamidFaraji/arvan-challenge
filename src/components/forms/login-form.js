import React from 'react';
import { Link } from 'react-router-dom';
import { withFormik, Form } from 'formik';
import { Card, CardBody, Button } from 'reactstrap';
import * as Yup from 'yup';
import Field from './field';

const LoginForm = (props) => (
  <Card>
    <CardBody>
      <Form>
        <Field
          name="email"
          label="Email"
          type="email"
          {...props}
        />
        <Field
          name="password"
          label="Password"
          type="password"
          {...props}
        />
        <Button block type="submit" color="success" size="large">
          {props.loading ? 'Loading...': 'Login'}
        </Button>
      </Form>
      <hr />
      <Button
        block
        tag={Link}
        size="large"
        tag={Link}
        to="/auth/register"
      >
        Register
      </Button>
    </CardBody>
  </Card>
);

export default withFormik({
  mapPropsToValues: props => ({
    email: '',
    password: ''
  }),
  validationSchema: Yup.object().shape({
    email: Yup.string().email('Email incorrect').required('Email is required'),
    password: Yup.string().min(8, 'Password must be 8 chars').required('Password is required')
  }),
  handleSubmit: (values, { props }) => {
    props.onSubmit(values);
  }
})(LoginForm);
