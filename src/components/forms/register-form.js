import React from 'react';
import { Link } from 'react-router-dom';
import { withFormik, Form } from 'formik';
import { Card, CardBody, Button } from 'reactstrap';
import * as Yup from 'yup';
import Field from './field';

const RegisterForm = props => (
  <Card>
    <CardBody>
      <Form>
        <Field
          name="username"
          label="Username"
          type="text"
          {...props}
        />
        <Field
          name="email"
          label="E-mail"
          type="email"
          {...props}
        />
        <Field
          name="password"
          label="Password"
          type="password"
          {...props}
        />
        <Button block type="submit" color="success" size="large">
          {props.loading ? 'Loading...': 'Register'}
        </Button>
      </Form>
      <hr />
      <Button
        block
        tag={Link}
        size="large"
        tag={Link}
        to="/auth/login"
      >
        Login
      </Button>
    </CardBody>
  </Card>
);

export default withFormik({
  mapPropsToValues: props => ({
    username: '',
    email: '',
    password: ''
  }),
  validationSchema: Yup.object().shape({
    username: Yup.string().required('Username is required'),
    email: Yup.string().email('Email incorrect').required('Email is required'),
    password: Yup.string().min(8, 'Password must be 8 chars').required('Password is required')
  }),
  handleSubmit: (values, { props }) => {
    props.onSubmit(values);
  }
})(RegisterForm);
