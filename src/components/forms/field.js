import React, { Component } from 'react';
import { Input, FormGroup, FormFeedback, FormText, Label } from 'reactstrap';

class Field extends Component {
  static defaultProps = {
    component: Input,
    name: '',
    type: 'text',
    label: '',
    value: '',
    error: null,
    hint: null,
    placeholder: ''
  };

  isInvalid() {
    const { name, errors, touched } = this.props;

    return errors[name] && touched[name];
  }

  render() {
    const {
      name,
      type,
      label,
      hint,
      placeholder,
      values,
      errors,
      handleChange,
      handleBlur,
      component
    } = this.props;

    return (
      <FormGroup>
        {label && <Label for={name}>{label}</Label>}
        {React.createElement(component, {
          name: name,
          type: type,
          id: name,
          invalid: this.isInvalid(),
          value: values[name],
          placeholder: placeholder,
          onChange: handleChange,
          onBlur: handleBlur
        })}
        {this.isInvalid() && (
          <FormFeedback invalid tooltip>
            {errors[name]}
          </FormFeedback>
        )}
        {hint && <FormText>{hint}</FormText>}
      </FormGroup>
    );
  }
}

export default Field;
