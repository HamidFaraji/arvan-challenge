import React, { Component } from 'react';
import { Provider } from 'mobx-react';
import { Router, Route, Switch } from 'react-router-dom';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createBrowserHistory } from 'history';
import { stores } from 'modules';
import axiosInstance from 'app/lib/axios-instance';
import Header from 'components/layout/header';

import Home from 'pages/home';
import Auth from 'pages/auth';
import Article from 'pages/article';
import User from 'pages/user';

export const history = createBrowserHistory();
export const request = axiosInstance(stores.authStore.token);

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Provider {...stores} request={request}>
        <Router history={history}>
          <main>
            <Helmet>
              <title>Arcan Cloud Challenge</title>
            </Helmet>
            <Header />
            <Container>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/auth" component={Auth} />
                <Route path="/article" component={Article} />
                <Route path="/user" component={User} />
              </Switch>
            </Container>
          </main>
        </Router>
      </Provider>
    );
  }
}
