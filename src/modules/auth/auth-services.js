import { request } from 'app';

const { API_ROOT } = process.env;

export async function loginApi(user) {
  const res = await request.post(`${API_ROOT}/users/login`, {
    user
  });

  return res.data;
}

export async function registerApi(user) {
  const res = await request.post(`${API_ROOT}/users`, {
    user
  });

  return res.data;
}