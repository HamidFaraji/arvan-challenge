import { observable, action, computed } from 'mobx';
import { loginApi, registerApi } from './auth-services';

export default class AuthStore {
  @observable id;
  @observable username = '';
  @observable token = '';
  @observable pending = false;
  @observable error = false;
  @observable messages = [];

  constructor() {
    const cachedId = window.localStorage.getItem('userId');
    const cachedToken = window.localStorage.getItem('userToken') || '';
    const cachedUsername = window.localStorage.getItem('userName');

    this.id = cachedId;
    this.username = cachedUsername;
    this.token = cachedToken;
  }

  @computed get isLogin() {
    return this.token.length > 1;
  }

  @action async login(email, password) {
    this.pending = true;

    try {
      const loginRes = await loginApi({
        email,
        password
      });

      if (loginRes.user) {
        this.setLogin(loginRes.user);
      } else {
        this.messages = loginRes.errors;

        throw new Error();
      }
    } catch(e) {
      this.error = true;
    } finally {
      this.pending = false;
    }
  }

  @action logout() {
    this.id = null;
    this.token = '';

    window.localStorage.removeItem('userId');
    window.localStorage.removeItem('userToken');
  }

  @action async register(username, email, password) {
    this.pending = true;

    try {
      const registerRes = await registerApi({
        username,
        email,
        password
      });

      if (registerRes.user) {
        this.setLogin(registerRes.user);
      } else {
        this.messages = registerRes.errors;

        throw new Error();
      }
    } catch(e) {
      this.error = true;
    } finally {
      this.pending = false;
    }
  }

  setLogin(user) {
    this.id = user.id;
    this.username = user.username;
    this.token = user.token;

    window.localStorage.setItem('userToken', this.token);
    window.localStorage.setItem('userId', this.id);
    window.localStorage.setItem('userName', this.username);
  }
}
