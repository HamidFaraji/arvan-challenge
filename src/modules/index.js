import ArticleStore from 'modules/article/article-store';
import TagStore from 'modules/tag/tag-store';
import AuthStore from 'modules/auth/auth-store';
import ProfileStore from 'modules/profile/profile-store';

export const articleStore = new ArticleStore();
export const tagStore = new TagStore();
export const authStore = new AuthStore();
export const profileStore = new ProfileStore();

export const stores = {
  articleStore,
  tagStore,
  authStore,
  profileStore
};
