import { observable, action } from 'mobx';
import { authStore } from 'modules';
import { history } from 'app';
import { fetchProfile, followUserApi, unfollowUserApi } from './profile-services';
import user from '../../pages/user/index';

export default class ProfileStore {
  @observable pending = false;
  @observable error = false;
  @observable following = false;
  @observable userProfile = {};

  @action async loadProfile(username) {
    this.pending = true;

    try {
      const userProfile = await fetchProfile(username);

      this.userProfile = userProfile;
      this.following = userProfile.following;
    } catch (e) {
      this.error = false;
    } finally {
      this.pending = false;
    }
  }

  @action async followUser(username) {
    this.pending = true;

    try {
      if (authStore.isLogin) {
        const followRes = await followUserApi(username);

        this.following = true;
      } else {
        history.push('/auth/login');
      }
    } catch (e) {
      this.error = true;
    } finally {
      this.pending = false;
    }
  }

  @action async unfollowUser(username) {
    this.pending = true;

    try {
      const unfollowRes = await unfollowUserApi(username);

      this.following = false;
    } catch (e) {
      this.error = true;
    } finally {
      this.pending = false;
    }
  }

  @action async updateProfile() {

  }
}