import { request } from 'app';

const { API_ROOT } = process.env;

export async function fetchProfile(username) {
  const res = await request.get(`${API_ROOT}/profiles/${username}`);

  return res.data.profile;
}

export async function followUserApi(username) {
  const res = await request.post(`${API_ROOT}/profiles/${username}/follow`);

  return res.data.profile;
}

export async function unfollowUserApi(username) {
  const res = await request.delete(`${API_ROOT}/profiles/${username}/follow`);

  return res.data.profile;
}
