import { request } from 'app';

const { API_ROOT } = process.env;

export async function fetchArticles(page, author, tag) {
  var articleParams = new URLSearchParams(`?limit=10&offset=${(page - 1) * 10}`);

  if (author) articleParams.append('author', author);
  if (tag) articleParams.append('tag', tag);

  const res = await request.get(`${API_ROOT}/articles?${articleParams}`);

  return res.data;
}

export async function fetchArticleDetail(slug) {
  const res = await request.get(`${API_ROOT}/articles/${slug}`);

  return res.data.article;
}

export async function likeArticleApi(slug) {
  const res = await request.post(`${API_ROOT}/articles/${slug}/favorite`);

  return res.data.article;
}
