import { observable, action, computed } from 'mobx';
import { fetchArticles, fetchArticleDetail, likeArticleApi } from './article-services';
import { history } from 'app';
import { authStore } from 'modules';

export default class ArticleStore {
  @observable pending = false;
  @observable error = false;
  @observable list = [];
  @observable currentDetail = {};
  @observable allCount = 0;
  @observable liked = [];

  @computed get count() {
    return this.list.length;
  }

  @action async loadArticles(page = 1, author, tag) {
    this.pending = true;

    const articles = await fetchArticles(page, author, tag);

    this.list = articles.articles;
    this.allCount = articles.articlesCount;
    this.pending = false;
  }

  @action async loadArticleDetail(slug) {
    this.pending = true;

    const article = await fetchArticleDetail(slug);

    this.currentDetail = article
    this.pending = false;
  }

  @action async likeArticle(slug) {
    try {
      if (authStore.isLogin) {
        const likeRes = await likeArticleApi(slug, authStore.token);

        if (likeRes) {
          this.liked.push(slug);
        }
      } else {
        history.push('/auth/login');
      }
    } catch(e) {
      console.log(e);
      this.error = true;
    }
  }
}
