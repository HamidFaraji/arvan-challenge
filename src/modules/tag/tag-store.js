import { observable, action, computed } from 'mobx';
import { fetchTags } from './tag-services';

export default class TagStore {
  @observable list = [];
  @observable pending = false;
  @observable error = false;
  
  @computed get count() {
    return this.list.length;
  }

  @action async loadTags() {
    this.pending = true;

    try {
      const articles = await fetchTags();
      this.list = articles;
    } catch(e) {
      this.error = true;
    } finally {
      this.pending = false;
    }
  }
}
