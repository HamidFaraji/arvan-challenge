import { request } from 'app';

const { API_ROOT } = process.env;

export async function fetchTags() {
  const res = await request.get(`${API_ROOT}/tags`);

  return res.data.tags;
}
