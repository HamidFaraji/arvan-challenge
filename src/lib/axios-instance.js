import axios from 'axios';

function axiosInstance(token) {
  const { API_BASE } = process.env;

  let headers = {
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
  };

  if (token) {
    headers['Authorization'] = `Token ${token}`;
  }

  return axios.create({
    baseURL: API_BASE,
    responseType: 'json',
    headers
  });
}

export default axiosInstance;
