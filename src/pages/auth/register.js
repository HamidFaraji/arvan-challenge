import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Redirect } from 'react-router-dom';
import { Row, Col, Alert } from 'reactstrap';
import RegisterForm from 'components/forms/register-form';

@inject('authStore')
@observer
export default class Register extends Component {
  constructor(props) {
    super(props);

    this.handleRegisterFormSubmit = this.handleRegisterFormSubmit.bind(this);
  }

  handleRegisterFormSubmit({ username, email, password }) {
    this.props.authStore.register(username, email, password);
  }

  render() {
    const { authStore } = this.props;

    if (authStore.isLogin) {
      return <Redirect to="/user/dashboard" />;
    }

    return (
      <section className="register-page">
        <Row>
          <Col md={{ size:4, offset: 4 }}>
            {authStore.error && <Alert color="danger">Oops!!! Error while registeration...</Alert>}
            <RegisterForm
              loading={authStore.pending}
              onSubmit={this.handleRegisterFormSubmit}
            />
          </Col>
        </Row>
      </section>
    );
  }
}
