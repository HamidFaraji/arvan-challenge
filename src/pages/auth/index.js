import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';

import Login from 'pages/auth/login';
import Register from 'pages/auth/register';

export default () => (
  <Fragment>
    <Route path="/auth/login" component={Login} />
    <Route path="/auth/register" component={Register} />
  </Fragment>
);
