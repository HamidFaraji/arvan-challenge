import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Redirect } from 'react-router-dom';
import { Row, Col, Alert } from 'reactstrap';
import LoginForm from 'components/forms/login-form';
import typy from 'typy';

@inject('authStore')
@observer
export default class Login extends Component {
  constructor(props) {
    super(props);

    this.handleLoginFormSubmit = this.handleLoginFormSubmit.bind(this);
  }

  handleLoginFormSubmit({ email, password }) {
    this.props.authStore.login(email, password);
  }

  render() {
    const { authStore } = this.props;
    const afterLogin = typy(location, 'state.afterLogin').safeObject || '/user/dashboard/';

    if (authStore.isLogin) {
      return <Redirect to={afterLogin} />;
    }

    return (
      <section className="login-page">
        <Row>
          <Col md={{ size:4, offset: 4 }}>
            {authStore.error && <Alert color="danger">Oops!!! Email or password incorrect...</Alert>}
            <LoginForm
              loading={authStore.pending}
              onSubmit={this.handleLoginFormSubmit}
            />
          </Col>
        </Row>
      </section>
    );
  }
}
