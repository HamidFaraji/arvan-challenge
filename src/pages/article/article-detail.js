import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Card, CardText, CardTitle, Button, CardSubtitle } from 'reactstrap';
import Loading from 'components/loading';
import typy from 'typy';

@inject('articleStore')
@observer class ArticleDetail extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { slug } = this.props.match.params;
    this.props.articleStore.loadArticleDetail(slug);
  }

  render() {
    const { articleStore } = this.props;
    const article = articleStore.currentDetail;
    const username = typy(article, 'author.username').safeObject;

    if (articleStore.pending) {
      return <Loading />;
    }

    return (
      <section className="article-page">
        <Container>
          <Row>
            <Col md="12">
              <Card
                body
                className="articles-list__item"
              >
                <CardTitle>{article.title}</CardTitle>
                <CardSubtitle>
                  <span className="articles-list__author">
                    <span>By: </span>
                    <strong>
                      <Link to={`/user/@${username}`}>
                        {username}
                      </Link>
                    </strong>

                  </span>
                  <span className="articles-list__date">
                    <span>Date: </span>
                    <strong>
                      {new Date(article.createdAt).toDateString()}
                    </strong>

                  </span>
                </CardSubtitle>
                <div className="articles-list__body">
                  <CardText>{article.description}</CardText>
                </div>
                <div className="clearfix">
                  {
                    <Button
                      color="danger"
                      className="articles-list__like-btn"
                      onClick={() => articleStore.likeArticle(article.slug)}
                      disabled={article.favorited || articleStore.liked.indexOf(article.slug) > -1}
                    >
                      <i className="material-icons md-24">
                        {article.favorited || articleStore.liked.indexOf(article.slug) > -1 ? 'favorite' : 'favorite_border'}
                      </i>
                    </Button>
                  }
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </section>
    );
  }
}

export default ArticleDetail;
