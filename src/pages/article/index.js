import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';

import ArticleDetail from './article-detail';

export default () => (
  <Fragment>
    <Route path="/article/:slug" component={ArticleDetail} />
  </Fragment>
);
