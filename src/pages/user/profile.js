import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import Helmet from 'react-helmet';
import classnames from 'classnames';
import { Jumbotron, Button, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import LatestArticles from 'containers/latest-articles';
import Loading from 'components/loading';

@inject('profileStore')
@observer class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: '1'
    };

    this.toggleFollow = this.toggleFollow.bind(this);
  }

  componentDidMount() {
    const { match, profileStore } = this.props;

    profileStore.loadProfile(match.params.username);
  }

  toggleTab(activeTab) {
    this.setState({
      activeTab
    })
  }

  toggleFollow(following, username) {
    const { profileStore } = this.props;

    if (following) {
      profileStore.unfollowUser(username);
    } else {
      profileStore.followUser(username);
    }
  }

  render() {
    const { activeTab } = this.state;
    const { match, profileStore } = this.props;
    const { username } = match.params;
    const userProfile = profileStore.userProfile;
    const following = profileStore.following;

    if (profileStore.pending) {
      return <Loading />
    }

    return (
      <section className="profile-page">
        <Helmet>
          <title>{`${username}'s Profile`}</title>
        </Helmet>
        <Jumbotron className="clearfix">
          <img src={userProfile.image} className="float-left img-thumbnail rounded mr-2" width="64" />
          <h1 className="display-6 float-left">{username}</h1>
          <Button
            color={following ? 'danger' : 'primary'}
            className="float-right"
            onClick={() => this.toggleFollow(following, username)}
          >
            {following ? 'Unfollow User' : 'Follow User'}
          </Button>
        </Jumbotron>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '1' })}
              onClick={() => this.toggleTab('1')}
            >
              {username} Articles
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <LatestArticles location={location} author={username} />
          </TabPane>
        </TabContent>
      </section>
    );
  }
}

export default Profile;
