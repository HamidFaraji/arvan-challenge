import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';

import Dashbarod from 'pages/user/dashboard';
import Profile from 'pages/user/profile';

export default () => (
  <Fragment>
    <Route path="/user/dashboard" component={Dashbarod} />
    <Route path="/user/@:username" component={Profile} />
  </Fragment>
);
