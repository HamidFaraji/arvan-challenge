import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Container, Row, Col } from 'reactstrap';
import LatestArticles from 'containers/latest-articles';
import TagsList from 'components/tags-list';

@inject('articleStore', 'tagStore')
@observer class Home extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.tagStore.loadTags();
  }

  render() {
    const { tagStore, location } = this.props;

    return (
      <section className="home-page">
        <Container>
          <Row>
            <Col md="8">
              <LatestArticles location={location} />
            </Col>
            <Col md="4">
              <TagsList list={tagStore.list} loading={tagStore.pending} />
            </Col>
          </Row>
        </Container>
      </section>
    );
  }
}

export default Home;
