import 'babel-polyfill';
import 'app/styles/bootstrap/bootstrap.scss';
import 'app/styles/main.scss';
import React from 'react';
import { render } from 'react-dom';
import App from 'app';

window.env = process.env.NODE_ENV;

render(
  <App />,
  document.getElementById('root')
);
