# Arvan Cloud Challenge
Port of github realworld project written in React + MobX + React Router

## Installation

- Clone the repo
- `cd` to folder and run `npm install`
- rename `.env_sample` to `.env`
- run `npm run dev` and wait to webpack build bundle file
- navigate to `http://localhost:3000` to see it


## Technologies

- **View Framework**: React
- **State Manager**: MobX
- **HTTP Handler**: Axios
- **Form Organizer**: Formik
- **UI Framework**: Bootstrap [Reactstrap]
- **Router**: React Router 4
- **Schema Builder**: Yup


## Routes
| Address | Description | Protected
| -- | -- | -- |
| / | Home Page | NO |
| /auth/login | Login to site | NO |
| /auth/register | Register to site | NO |
| /user/dashboard | User dashboard | YES |
| /user/@USERNAME | Every registred user profile | NO |
| /article/ARTICLE_SLUG | Article detail | NO |