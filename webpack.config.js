const webpack = require('webpack');
const path = require('path');
const dotenv = require('dotenv');
const HtmlWebpackPlugin = require('html-webpack-plugin');

dotenv.config({
  path: '.env'
});

const env = Object.keys(process.env).reduce((acc, current) => {
  acc[`process.env.${current}`] = JSON.stringify(process.env[current]);

  return acc;
}, {});

const config = {
  entry: path.join(__dirname, '/index.js'), 
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'bundle.js'
  },
  mode: process.env.NODE_ENV || 'development',
  devtool: process.env.NODE_ENV === 'development' ? 'eval-source-map' : 'none',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.(scss|sass)$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(ttf|eot|woff|woff2|png|jpg|jpeg|gif|svg)$/,
        use: {
          loader: 'file-loader'
        },
      }
    ],
  },
  resolve: {
    alias: {
      assets: path.resolve(__dirname, 'src/assets'),
      modules: path.resolve(__dirname, 'src/modules'),
      components: path.resolve(__dirname, 'src/components'),
      containers: path.resolve(__dirname, 'src/containers'),
      pages: path.resolve(__dirname, 'src/pages'),
      app: path.resolve(__dirname, 'src')
    }
  },
  plugins: [
    new webpack.DefinePlugin(env),
    new HtmlWebpackPlugin({
      template: 'dist/index.html'
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, '/dist'),
    compress: true,
    port: 3000,
    publicPath: '/',
    historyApiFallback: true
  },
};

module.exports = config;
